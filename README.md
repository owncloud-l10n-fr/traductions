# Traductions FR d'Owncloud

Le projet a un dépot vide car il sert principalement pour son système de tickets (issues) et son
wiki. Dans la partie wiki, vous trouverez des aides sur comment « bien » participer à l'effort
de traduction, des informations concernant les différents outils utilisés par l'équipe etc.

Dans la partie « Issues », une liste de tâches à effectuer pour y voir plus clair ce qu'il faut
faire.

N'hésitez pas si vous avez des questions à envoyer un mail sur la liste de diffusion !